﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;

namespace macro
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread m_BackGroundThread = null;
        List<Keys[]> m_Keys;

        static public event EventHandler Started;
        static public void OnStarted(object sender, EventArgs evt)
        {
            if (Started != null)
                Started(sender, evt);
        }
        static public event EventHandler Overed;
        static public void OnOvered(object sender, EventArgs evt)
        {
            if (Overed != null)
                Overed(sender, evt);
        }

        public MainWindow()
        {
            InitializeComponent();

            m_Keys = new List<Keys[]>();

            Started += (s, e) =>
            {
                Dispatcher.Invoke(() =>
                {
                    btnAdd.IsEnabled = false;
                    btnStart.IsEnabled = false;
                    targettbox.IsEnabled = false;
                    cycletbox.IsEnabled = false;
                    btnEnd.IsEnabled = true;
                });
            };
            Overed += (s, e) =>
            {
                Dispatcher.Invoke(() =>
                {
                    btnAdd.IsEnabled = true;
                    btnStart.IsEnabled = true;
                    targettbox.IsEnabled = true;
                    cycletbox.IsEnabled = true;
                    btnEnd.IsEnabled = false;
                });
            };

            Closing += (s, e) =>
            {
                if (m_BackGroundThread != null && m_BackGroundThread.IsAlive)
                    m_BackGroundThread.Abort();
            };

            btnAdd.Click += (s, e) =>
            {
                KeyDialog tmp = new KeyDialog();
                if (tmp.ShowDialog() == true)
                {
                    m_Keys.Add(tmp.Keys);
					keyList.Items.Add(tmp.Text);
                    keyList.Items.Refresh();
                }
            };
            btnStart.Click += (s, e) =>
            {
                if (m_BackGroundThread != null && m_BackGroundThread.IsAlive)
                    m_BackGroundThread.Abort();
                m_BackGroundThread = new Thread(() =>
                {
                    double term = 0;
                    Process[] proc = null;

                    Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            term = double.Parse(cycletbox.Text);
                            term *= 1000;
                        }
                        catch (Exception exc)
                        {
                            System.Windows.MessageBox.Show("시간 입력을 올바르게 하세요.\n(숫자만 입력)");
                            return;
                        }
                        proc = Process.GetProcessesByName(targettbox.Text);
                        if (proc.Length < 1)
                            return;

                    });

                    OnStarted(this, EventArgs.Empty);
                    while (true)
                    {
                        foreach (var p in proc)
                        {
                            Debug.WriteLine(p.ProcessName);
                            if (GetForegroundWindow() == p.MainWindowHandle)
                            {
                                foreach (var ks in m_Keys)
                                    ExeKeys(ks);
                            }
                        }
                        Thread.Sleep((int)Math.Floor(term));
                    }
                });
                m_BackGroundThread.Start();
            };
            btnEnd.Click += (s, e) =>
            {
                if (m_BackGroundThread != null && m_BackGroundThread.IsAlive)
                    m_BackGroundThread.Abort();
                OnOvered(this, EventArgs.Empty);
            };
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            int idx = keyList.SelectedIndex;
            if (idx >= 0)
            {
				keyList.Items.RemoveAt(idx);
                keyList.Items.Refresh();
            }
        }
        #region 가상키보드 이벤트 부분
        int KEY_DOWN = 0x00;
        int KEY_EXTENDED = 0x01;
        int KEY_UP = 0x02;
        // 가상키보드 값을 발생시키기 위한 부분
        [DllImport("user32.dll")]
        static extern void keybd_event(byte vk, byte scan, int flags, int extrainfo);
        // 키를 눌렸다 떼는 함수
        private void ExeKeys(Keys[] keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                keybd_event((byte)keys[i], 0, KEY_DOWN, 0);
            }
            for (int i = keys.Length - 1; i >= 0; i--)
            {
                keybd_event((byte)keys[i], 0, KEY_UP, 0);
            }
        }
        #endregion

        // 현재 포커싱(활성화) 돼있는 프로세서를 알아낸다.
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();
    }

}
