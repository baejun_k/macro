﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace macro
{
    /// <summary>
    /// Window1.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class KeyDialog : Window
    {
        // 결과 배열
        static List<Keys> result;

        // 키보드 입력 시 발생할 이벤트 정의
        static event EventHandler KeyPressed;
        static void OnKeyPressed(object sender, EventArgs evt)
        {
            if (KeyPressed != null)
                KeyPressed(sender, evt);
        }

        public KeyDialog()
        {
            InitializeComponent();
            result = new List<Keys>();
            
            System.Windows.Application curApp = System.Windows.Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.Width) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.Height) / 2;
            Debug.WriteLine(this.Height + " " + this.Width);

            KeyPressed += (s, e) =>
            {
                string tmp = result[0].ToString();
                for(int i = 1; i<result.Count; i++)
                {
                    tmp += (" + " + result[i].ToString());
                }
                this.Text = tmp;
                value.Text = this.Text;
            };

            _hookID = SetHook(_proc);
            Closing += KeyDialog_Closing;
        }

        private void KeyDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UnhookWindowsHookEx(_hookID);
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            if (result.Count > 0)
                this.DialogResult = true;
            else
                this.DialogResult = false;
        }

        public Keys[] Keys {
            get {
                return result.ToArray();
            }
        }
        public string Text {
            get;set;
        }

        // keyboard hooking을 위해서
        // winapi user32.dll 부분을 불러온다.
        #region wParam values
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        // "alt"키 == system 키
        // "alt"키 + "any key" (alt 조합키) == system 키
        private const int WM_SYSKEYDOWN = 0x0104;
        private const int WM_SYSKEYUP = 0x0105;
        #endregion
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;
        // lParam의 구조체
        [StructLayout(LayoutKind.Sequential)]
        private struct KBDLLHOOKSTRUCT
        {
            public Keys key;
            public int scanCode;
            public int flags;
            public int time;
            public IntPtr extra;
        }
        // hook 설정
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        // 키보드 값을 읽어 내는 부분
        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            Debug.WriteLine((int)wParam);
            if (nCode >= 0 && 
                ((int)wParam == WM_KEYDOWN || (int)wParam == WM_SYSKEYDOWN))
            {
                KBDLLHOOKSTRUCT objKeyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));
                // 눌려진 키를 저장
                result.Add(objKeyInfo.key);
                OnKeyPressed(null, EventArgs.Empty);
            }
            // 키를 입력받을 때 동작을 하지 않게 하기 위해서 이 값을 리턴
            // ex] win키를 막는다.
            return (IntPtr)1;
            // 키보드 이벤트를 정상으로 작동하기 위해서는 아래를 리턴해준다.
            // CallNextHookEx(_hookID, nCode, wParam, lParam)
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);
        // hook 해제
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);
        // 값을 다음으로 전달
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);
    }
}
